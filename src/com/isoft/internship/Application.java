package com.isoft.internship;

import com.isoft.internship.manager.CommandManager;
import com.isoft.internship.manager.CommandManagerImpl;

public class Application {
	
	public static void main(String[] args) {
		CommandManager manager = new CommandManagerImpl();
		manager.run();
	}
}
