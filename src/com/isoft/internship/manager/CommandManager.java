package com.isoft.internship.manager;

import com.isoft.internship.entity.HuffmanNode;

public interface CommandManager {
	String encode(String text);
	
	HuffmanNode buildTree();
	
	void setPrefixCodes(HuffmanNode node, StringBuilder builder);
	
	Integer calcTreeDepth();
	
	void run();
}
