package com.isoft.internship.manager;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

import com.isoft.internship.constants.GlobalConstants;
import com.isoft.internship.entity.HuffmanNode;
import com.isoft.internship.entity.UniqueCharacterCounter;
import com.isoft.internship.util.CustomQueueComparator;

public class CommandManagerImpl implements CommandManager {
	private static Map<Character, String> charPrefixHashMap = new HashMap<>();
	private UniqueCharacterCounter counter;

	public CommandManagerImpl() {

	}

	public CommandManagerImpl(UniqueCharacterCounter counter) {
		this.counter = counter;
	}

	public UniqueCharacterCounter getCounter() {
		return counter;
	}

	@Override
	public String encode(String text) {
		this.getCounter().increment(text);
		System.out.println(GlobalConstants.CHARACTER_FREQUENCY_TEXT + counter.getUniqueCharFreq());

		// build the tree and return the root
		HuffmanNode root = buildTree();
		if (root != null) {
			System.out.println(GlobalConstants.ROOT_VALUE + root.getFrequency());
		}

		// set 0 and 1 for the nodes
		setPrefixCodes(root, new StringBuilder());
		System.out.println(GlobalConstants.CHARACTER_PREFIX_TEXT + charPrefixHashMap);

		StringBuilder buildEncodedText = new StringBuilder();
		buildEncodedText.append(GlobalConstants.BUILDER_APPEND_TEXT);

		// append the whole prefixCode for the sentence
		text.chars().forEach(value -> buildEncodedText.append(charPrefixHashMap.get((char) value)));

		return buildEncodedText.toString();
	}

	@Override
	public HuffmanNode buildTree() {
		// invoking a queue, which keeps the node in ascending order
		Queue<HuffmanNode> priorityQueue = new PriorityQueue<>(new CustomQueueComparator());
		Set<Character> keySet = this.getCounter().getCharacters();
		Integer charFrequency;
		HuffmanNode node;

		// build all nodes and add them to the queue
		for (Character character : keySet) {
			charFrequency = this.getCounter().getUniqueCharFreq().get(character);
			node = new HuffmanNode(character, charFrequency, null, null);
			priorityQueue.offer(node);
		}

		HuffmanNode leftNode;
		HuffmanNode rightNode;
		Integer sumFrequency;
		HuffmanNode sumNode;

		// make new summed nodes and add them back again to the queue
		while (priorityQueue.size() > 1) {
			leftNode = priorityQueue.poll();
			rightNode = priorityQueue.poll();

			sumFrequency = leftNode.getFrequency() + rightNode.getFrequency();
			sumNode = new HuffmanNode(GlobalConstants.SUMMED_NODE_CHAR, sumFrequency, leftNode, rightNode);

			priorityQueue.offer(sumNode);
		}

		return priorityQueue.poll();

	}

	@Override
	public void setPrefixCodes(HuffmanNode node, StringBuilder builder) {
		if (node != null) {
			// checking if it is a leaf
			if (node.getLeftNode() == null && node.getRightNode() == null) {
				charPrefixHashMap.put(node.getCharacter(), builder.toString());
			} else {
				builder.append(GlobalConstants.ZERO_PREFIX);
				setPrefixCodes(node.getLeftNode(), builder);
				builder.deleteCharAt(builder.length() - 1);

				builder.append(GlobalConstants.ONE_PREFIX);
				setPrefixCodes(node.getRightNode(), builder);
				builder.deleteCharAt(builder.length() - 1);
			}
		}

	}

	@Override
	public Integer calcTreeDepth() {
		Optional<String> maxStringPrefixLength = charPrefixHashMap.values().stream()
				.max(Comparator.comparing(String::length));
		if (maxStringPrefixLength.isPresent()) {
			// returns the character with max prefix length + the node
			return maxStringPrefixLength.get().length() + 1;
		}
		return 0;
	}

	@Override
	public void run() {
		UniqueCharacterCounter charCounter = new UniqueCharacterCounter();
		CommandManager manager = new CommandManagerImpl(charCounter);

		Scanner scan = new Scanner(System.in);
		System.out.print(GlobalConstants.ENTER_TEXT);
		String text = scan.nextLine();

		String encodedText = manager.encode(text);
		System.out.println(encodedText);
		System.out.println(GlobalConstants.ENCODED_TEXT_LENGTH
				+ (encodedText.length() - GlobalConstants.BUILDER_APPEND_TEXT.length()));

		Integer treeDepth = manager.calcTreeDepth();
		System.out.println(GlobalConstants.TREE_DEPTH_TEXT + treeDepth);
		scan.close();
	}
}
