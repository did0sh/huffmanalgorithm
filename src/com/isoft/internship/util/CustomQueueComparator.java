package com.isoft.internship.util;

import java.util.Comparator;

import com.isoft.internship.entity.HuffmanNode;

public class CustomQueueComparator implements Comparator<HuffmanNode> {

	@Override
	public int compare(HuffmanNode first, HuffmanNode second) {
		return Integer.compare(first.getFrequency(), second.getFrequency());
	}

}
