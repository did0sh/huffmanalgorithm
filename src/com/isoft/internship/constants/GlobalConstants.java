package com.isoft.internship.constants;

public class GlobalConstants {
	
	private GlobalConstants() {}
	
	public static final String ENTER_TEXT = "Enter text: ";
	public static final String ENCODED_TEXT_LENGTH = "Encoded text length: ";
	public static final String CHARACTER_FREQUENCY_TEXT = "Character Frequency Map: ";
	public static final String CHARACTER_PREFIX_TEXT = "Character Prefix Map: ";
	public static final String BUILDER_APPEND_TEXT = "Encoded: ";
	public static final String TREE_DEPTH_TEXT = "Tree depth: ";
	public static final String ROOT_VALUE = "Root value: ";
	public static final Character ZERO_PREFIX = '0';
	public static final Character ONE_PREFIX = '1';
	public static final Character SUMMED_NODE_CHAR = '+';

}
