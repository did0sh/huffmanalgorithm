package com.isoft.internship.entity;

public class HuffmanNode implements Comparable<HuffmanNode> {
	
	private char ch;
	private int frequency;
	private HuffmanNode left;
	private HuffmanNode right;
	
	public HuffmanNode(char ch, int frequency, HuffmanNode left, HuffmanNode right) {
		this.ch = ch;
		this.frequency = frequency;
		this.left = left;
		this.right = right;
	}

	public int getFrequency() {
		return frequency;
	}

	public char getCharacter() {
		return ch;
	}

	public HuffmanNode getLeftNode() {
		return left;
	}

	public HuffmanNode getRightNode() {
		return right;
	}

	@Override
	public int compareTo(HuffmanNode otherNode) {
		return this.getFrequency() - otherNode.getFrequency();
	}

	@Override
	public String toString() {
		return String.format("%s - %d", this.getCharacter(), this.getFrequency());
	}
}
