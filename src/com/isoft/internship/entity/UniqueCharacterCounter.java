package com.isoft.internship.entity;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Keeps a running count of how many times each unique character is seen.
 */
public class UniqueCharacterCounter {

	private final Map<Character, Integer> uniqueCharsFreq = new HashMap<>();

	/**
	 * Increments the count of the given character, setting it to one on first
	 * appearance.
	 *
	 * @param c the character to count
	 */
	public void increment(Character c) {
		Integer freq = uniqueCharsFreq.get(c);
		if (freq == null) {
			uniqueCharsFreq.put(c, 1);
		} else {
			uniqueCharsFreq.put(c, freq + 1);
		}
	}
	
	 /**
     * Increments the count of each character in the given text.
     *
     * @param text contains the characters to count
     */
	public void increment(String text) {
		char[] allCharacters = text.toCharArray();
		for (char ch : allCharacters) {
			increment(ch);
		}
	}
	
	 /**
     * Returns the set of characters seen.
     *
     * @return set containing each character seen while counting
     */
    public Set<Character> getCharacters() {
        return this.uniqueCharsFreq.keySet();
    }
    
    /**
     * Returns the unique characters with their frequency.
     *
     * @return collection containing all characters
     */
	public Map<Character, Integer> getUniqueCharFreq() {
		return Collections.unmodifiableMap(this.uniqueCharsFreq);
	}
	
}
