## Huffman Encoding Algorithm

This is basically a simple implementation of
the "Huffman" encoding algorithm, which does the following things:

1. Receives an **input text** from the user.
2. **Builds** a binary tree with **HuffmanNodes**.
3. **Encodes** the sentence in **prefix codes**.
4. **Calculates the length** of the **encoded text**.
5. **Calculates the depth** of the **created tree**.
6. Prints the **unique characters with their frequency count**.
7. Prints the **unique characters with their prefix codes**.
8. Prints the **encoded representation of the text/sentence**.
9. Prints the **encoded text length**.
10. Prints the **tree depth**.

---